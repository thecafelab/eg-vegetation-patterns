# code: Contains the full code for analysis. There are two main files. 'udf.R' and 'code.R' contains the user defined functions and the main code executed respectively. The file 'mcmc_post[original code_for reference].R' is an old code used for the original analysis

# data: Contains the base data for analysis. It has the species distribution data and the environmental variables data

# models: Contains the fitted model output for the four life-forms and two predictors- climate and topography as saved .RData files

# results: Meant to collect the code output and organised into 'pca' for PCA of environmental variables; 'diagnostic' for diagnosing fitted model; and 'main' for the figures of the manuscript
