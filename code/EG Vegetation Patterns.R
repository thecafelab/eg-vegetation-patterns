if (!dir.exists("code")) source("udf.R") else source("code/udf.R")

setwd(paste0(dirname(rstudioapi::getSourceEditorContext()$path), "/../"))
load(".RData")

##### Load environmental variables #####
DataType <- c("numeric", "text", "numeric", "numeric", "text", "text")
DataName <- c("ID", "Plot", "Lon", "Lat", "Species", "Life_form")
EnvType <- c("text", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric", "numeric")
EnvName <- c("Plot", "Lon", "Lat", "Prec_Cold_Quarter", "Prec_Warm_Quarter", "Prec_Dry_Quarter", "Prec_Wet_Quarter", "Prec_Seasonality", "Prec_Driest_Month", "Prec_Wettest_Month", "Prec_Annual", "Temp_Mean_Cold_Quarter", "Temp_Mean_Warm_Quarter", "Temp_Mean_Dry_Quarter", "Temp_Mean_Wet_Quarter", "Temp_Range_Annual", "Temp_Min_Coldest_Month", "Temp_Max_Warmest_Month", "Temp_Seasonality", "Isothermality", "Temp_Mean_Diurnal_Range", "Temp_Mean_Annual", "Climatic_Water_Deficit", "Elevation", "Aspect", "Slope")

Environment <- as.data.frame(unclass(readxl::read_excel("data/Environmental variables.xlsx", sheet = "env", col_types = EnvType)))
colnames(Environment) <- EnvName
Environment <- Environment[, c(1:3, 11, 7:6, 5:4, 10:8, 23:22, 13:12, 14:15, 18:17, 16, 21:20, 19, 24, 26:25)]

Environment <- cbind(Environment[, 1:3], transform(data = Environment, start = 4)[["Data_Transformed"]])

##### Correlation #####
library(PerformanceAnalytics)
tiff("results/Diagnostic/Correlation.tiff", width = 15, height = 15, units = "cm", res = 600)
chart.Correlation(Environment[, start:(ncol(Environment) - start + 1)], method = "pearson", histogram = TRUE, pch = 19)
dev.off()

##### PCA #####
library(factoextra)
results <- prcomp(Environment[, start:(ncol(Environment) - start + 1)], scale = TRUE, center = TRUE)
Environment <- cbind(Environment, as.data.frame(get_pca_ind(results)$coord[, 1:3]))
colnames(Environment)[(ncol(Environment) - 2):(ncol(Environment))] <- c("PC1", "PC2", "PC3")

tiff("results/Main/Correlation_Half.tiff", width = 15, height = 15, units = "cm", res = 600)
corrplot(cor(Environment[, 24:ncol(Environment)]), p.mat = corr.test(Environment[, 24:ncol(Environment)]), sig.level = 0.01, insig = "blank", type = "upper", order = "original", diag = FALSE, method = "color", col = colorRampPalette(c("red", "orange", "yellow", "white", "blue"))(nrow(Environment[, 24:ncol(Environment)])*ncol(Environment[, 24:ncol(Environment)])), addCoef.col = "black", addgrid.col = "grey", tl.cex = 0.8, cl.cex = 0.8)
dev.off()

write.csv(get_pca_var(results)$coord[, 1:3], file = "results/PCA/Parameters contribution.csv")
write.csv(get_pca_ind(results)$coord[, 1:3], file = "results/PCA/Sample contribution.csv")

data <- Environment[, start:23]
colnames(data) <- c("PrAnn", "PrWetQ", "PrDryQ", "PrWarmQ", "PrColdQ", "PrWetM", "PrDryM", "PrSeason", "CWD", "TempMeanAnn", "TempMeanWarmQ", "TempMeanColdQ", "TempMeanDryQ", "TempMeanWetQ", "TempMaxWarmM", "TempMinColdM", "TempRangeAnn", "TempMeanDiurRange", "Isothermality", "TempSeason")
results <- prcomp(data, scale = TRUE, center = TRUE)
rm(data)
options(ggrepel.max.overlaps = Inf)
ar <- as.numeric(unlist(strsplit("4:3", ":")))

a <- fviz_pca_varc(results, axes = c(1, 2), col.var ="contrib", gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"), labelsize = 3, repel = TRUE) + labs(x = paste0("Dimension 1 (", format(round(summary(results)$importance[2,][1]*100, digits = 1), nsmall = 1, scientific = FALSE), "%)"), y = paste0("Dimension 2 (", format(round(summary(results)$importance[2,][2]*100, digits = 1), nsmall = 1, scientific = FALSE), "%)")) + theme_bw() + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.ticks.length = unit(0.15, "cm"), axis.text.x = element_text(colour ="black"), axis.text.y = element_text(colour ="black")) + theme(legend.position ="none", axis.text = element_text(size = 12.5), axis.title = element_text(size = 15), plot.title = element_text(size = 18))
b <- fviz_pca_indc(results, axes = c(1, 2), col.ind ="cos2", gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"), label = FALSE, labelsize = 5, repel = TRUE) + scale_x_continuous(limits = c(-2.0, 4.5), breaks = seq(-2.0, 4.5, 2.0)) + scale_y_continuous(limits = c(-1.5, 1.0), breaks = seq(-1.5, 1.0, 0.5)) + labs(x = paste0("Dimension 1 (", format(round(summary(results)$importance[2,][1]*100, digits = 1), nsmall = 1, scientific = FALSE), "%)"), y = paste0("Dimension 2 (", format(round(summary(results)$importance[2,][2]*100, digits = 1), nsmall = 1, scientific = FALSE), "%)")) + theme_bw() + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.ticks.length = unit(0.15, "cm"), axis.text.x = element_text(colour ="black"), axis.text.y = element_text(colour ="black")) + theme(legend.position ="none", axis.text = element_text(size = 12.5), axis.title = element_text(size = 15), plot.title = element_text(size = 18))
c <- fviz_eigc(results) + theme_bw() + theme(panel.grid.major = element_blank(), panel.grid.minor = element_blank(), axis.ticks.length = unit(0.15, "cm"), axis.text.x = element_text(colour ="black"), axis.text.y = element_text(colour ="black"), panel.background = element_rect(fill ="transparent", colour = NA), plot.background = element_rect(fill ="transparent", colour = NA), panel.border = element_rect(fill = NA, colour ="black", linewidth = 2)) + theme(legend.position ="none", axis.text = element_text(size = 22.5), axis.title = element_text(size = 22.5), plot.title = element_text(size = 25, hjust = 0.9, vjust = -5.5))

ggsave(a, file = paste0("results/PCA/Variable contribution", ".tiff"), width = 15.92, height = 15.92/ar[1]*ar[2], units = "cm", dpi = 600)
ggsave(b, file = paste0("results/PCA/Species contribution", ".tiff"), width = 15.92, height = 15.92/ar[1]*ar[2], units = "cm", dpi = 600)
ggsave(c, file = paste0("results/PCA/Scree plot", ".png"), bg = "transparent", width = 21, height = 21, units = "cm", dpi = 600)

##### Load data #####
Data <- readxl::read_excel("data/EG_species_latlong-new.xlsx", sheet = "specieslist", col_types = DataType) %>%
  as.data.frame() %>%
  setNames(DataName) %>%
  # Filter Data to retain unique plots
  filter(Plot %in% {
    x <- .
    x %>%
      distinct(Plot, .keep_all = TRUE) %>%
      select(Plot, Lon, Lat) %>%
      group_by(Lon, Lat) %>%
      filter(n() == 1) %>%
      ungroup() %>%
      pull(Plot)
  }) %>%
  # Create the species presence/absence matrix
  distinct(Plot, Species, .keep_all = TRUE) %>%
  mutate(Presence = 1) %>%
  left_join(Environment %>% select(-c(Lon, Lat)), by = "Plot") %>%
  filter(complete.cases(.)) %>%
  mutate(Life_form = stringr::str_to_title(Life_form)) %>%
  select(-ID) %>%
  relocate(Presence, .after = last_col()) %>%
  mutate(Life_form = factor(Life_form, levels = c("Tree", "Shrub", "Climber", "Herb"))) %>%
  split(.$Life_form) %>%
  map(~ .x %>%
        select(-Life_form) %>%
        pivot_wider(names_from = Species, values_from = Presence)
  ) %>%
  map(~ .x %>%
        mutate(across(4:ncol(.x), ~replace_na(., 0)))
  )

##### Initialize models #####
hmsc_clim <- Map(function(data, name) unfitted_model(data = data, x = c("PC1", "PC2", "PC3"), model_name = name, model_type = "clim"), Data, names(Data))

hmsc_topo <- Map(function(data, name) unfitted_model(data = data, x = c("Slope", "Aspect"), model_name = name, model_type = "topo"), Data, names(Data))

##### Sampling MCMC #####
samples <- c(1000)

# Create all combinations of model_name, model_type and samples
combinations <- expand.grid(
  model_name = c("Tree", "Shrub", "Herb", "Climber"),
  model_type = c("Clim", "Topo"),
  samples = samples
) %>%
  as_tibble() %>%
  arrange(model_name, model_type) %>%  # Arrange rows by model_name and model_type
  mutate(combo_name = paste(model_name, model_type, sep = "_")) %>%
  mutate(combo_name = factor(combo_name, levels = unique(combo_name), ordered = TRUE)) %>%
  split(.$combo_name) %>%  # Split data frame into a list based on combinations
  map(~ select(.x, -combo_name))

# Determine the number of chains and cores to use
chains <- as.numeric(availableCores() %/% length(combinations))
if (chains == 0) {
  chains <- 1
  cores <- availableCores()
} else {
  cores <- chains * length(combinations)
}

### !!!This part will takes weeks to process on a HPC!!! ###
timer("start", id = "sampling MCMC")

# Set up parallel processing
plan(multisession, workers = cores) # Adjust the number of workers as necessary
message(paste0("Deploying Gibbs Markov chain Monte Carlo (MCMC) with ", chains, " chains on ", nbrOfWorkers(), " parallel cores"))

# Apply the MCMC sampling to each combination using future_lapply
mcmc <- future_lapply(combinations, function(x) {
  model_name <- x$model_name %>% as.character()
  model_type <- x$model_type %>% as.character()
  samples <- x$samples %>% as.numeric()
  
  sample_mcmc(model_name, model_type, samples, chains)
})

plan(sequential)  # Reset the plan to sequential processing

timer("stop", id = "sampling MCMC")

save.image(".RData")

##### Read MCMC outputs; perform model diagnostics #####
# Create all combinations of (model_name, model_type)
combinations <- expand.grid(model_type = c("Clim", "Topo"), model_name = c("Tree", "Shrub", "Herb", "Climber"), stringsAsFactors = FALSE)

# Increase future memory cap to larger size, e.g. 1 GiB (1 * 1024^3 bytes)
options(future.globals.maxSize = 10 * 1024^3)

timer("start", id = "diagnosing models")

plan(multisession, workers = nrow(combinations))  # Set the number of workers/cores
message("Number of parallel cores: ", nbrOfWorkers())

# Each iteration processes a combination of (model_name, model_type)
model_diagnostics <- future_lapply(seq_len(nrow(combinations)), function(i) {
  # Extract the name & type for this iteration
  model_name <- combinations$model_name[i]  # "Tree", "Shrub", etc.
  model_type <- combinations$model_type[i]  # "Clim", "Topo", etc.
  
  # Pick the gradient columns conditionally
  if (model_type == "Clim") {
    x_data_cols <- c("PC1", "PC2", "PC3")  # e.g., for climate models
  } else if (model_type == "Topo") {
    x_data_cols <- c("Slope", "Aspect")  # e.g., for topography models
  } else {
    stop("Unknown model_type: ", model_type)  # fallback or raise an error
  }
  
  # Call diagnose_model() function
  diagnose_model(model_name = model_name, model_type = model_type, path = "models", samples = 1000, x_data = x_data_cols, psrf.beta_cutoff = 1.2, aspect_ratio = "4:3", dpi = 600)
}, future.seed = TRUE) %>%
  set_names(paste(combinations$model_name, combinations$model_type, sep = "_"))  # Name results list

# Switch back to sequential if you like
plan(sequential)

timer("stop", id = "diagnosing models")

##### Objective 1: Species association in relation to climate #####
timer("start", id = "model estimates")

plan(multisession, workers = length(model_diagnostics) / 2)  # Adjust this workers/cores carefully. This step is a memory intensive process. Each process might take up to 25 GB of RAM. Keep to 1 process for safety.
message("Number of parallel cores: ", nbrOfWorkers())

# Each iteration processes a combination of (model_name, model_type)
model_estimates <- future_lapply(seq_along(model_diagnostics), function(i) {
  evaluate_model(model_name = model_diagnostics[[i]]$model_name, model = model_diagnostics[[i]]$model, samples = 1000, div = 100, aspect_ratio = "4:3", dpi = 600)
}, future.seed = TRUE) %>%
  set_names(names(model_diagnostics))  # Name results list

# Switch back to sequential if you like
plan(sequential)

timer("stop", id = "model estimates")

##### Calculate and plot- occurrence predictions, species richness #####
# Create a temporary environment
temp_env <- new.env()

# Load all objects from the RData file into the temporary environment
load("original.RData", envir = temp_env)

# Extract the specific object, say "my_object"
prediction <- temp_env$predictions

rm(temp_env)
gc()

timer("start", id = "model predictions")

plan(multisession, workers = length(model_estimates))  # Set the number of workers/cores
message("Number of parallel cores: ", nbrOfWorkers())

model_predictions <- future_lapply(seq_along(model_estimates), function(i) {
  species_response(model_name = model_diagnostics[[i]]$model_name, model = model_diagnostics[[i]]$model, data = model_estimates[[i]]$beta_estimates, predictions = prediction[[i]], occ_cutoff = 0.00775, div = 100, aspect_ratio = "4:3", dpi = 600)
}, future.seed = TRUE) %>%
  set_names(names(model_estimates))  # Name results list

# Switch back to sequential if you like
plan(sequential)

timer("stop", id = "model predictions")


# Save output to .RData
save.image(".RData")

##### Create posterior estimates data for analysis #####
plan(multisession, workers = as.numeric(length(seq(1, length(model_estimates) - 1, 2))))
message("Number of parallel cores: ", nbrOfWorkers())

data <- future_lapply(seq(1, length(model_estimates) - 1, 2), function(i) {
  temp <- model_diagnostics[[i]]$model$XData
  tmp <- sapply(seq_len(model_diagnostics[[i]]$model$ns), function(k) {
    prediction[[i]][[1]][prediction[[i]][[1]][, 1] == unique(prediction[[i]][[1]][, 1])[k], 7]
  })
  temp[, 5:(model_diagnostics[[i]]$model$ns + 4)] <- tmp
  colnames(temp)[5:(model_diagnostics[[i]]$model$ns + 4)] <- unique(prediction[[i]][[1]][, 1])
  
  return(temp)
}) %>%
  set_names(names(prediction)[seq(1, length(model_estimates) - 1, 2)])

# Switch back to sequential
plan(sequential)

##### Calculate species niche width as area of convex hull/minimum convex polygon, niche optima #####
library(splancs)
Niche <- map_df(seq(1, length(model_estimates) - 1, 2), function(i) {
  map_df(5:(model_diagnostics[[i]]$model$ns + 4), function(j) {
    dat <- data[[(i + 1)/2]][data[[(i + 1)/2]][, j] > 0, c(2:3)]
    if (nrow(dat) <= 3) {
      df <- data.frame(Species = colnames(data[[(i + 1)/2]])[j], Life_form = names(data)[(i + 1)/2], Posterior_coefficients = model_estimates[[i]]$beta_estimates[[1]][(j - 4), 2], Optima_PC1_sq = 0, Optima_PC1 = 0, Optima_PC2 = 0, Niche_width = 0)
    } else {
      dd <- deldir::deldir(xy.coords(dat))
      cen.dd <- with(dd$summary, sapply(list(x, y), weighted.mean, del.wts))
      df <- data.frame(Species = colnames(data[[(i + 1)/2]])[j], Life_form = names(data)[(i + 1)/2], Posterior_coefficients = model_estimates[[i]]$beta_estimates[[1]][(j - 4), 2], Optima_PC1_sq = cen.dd[1]^2, Optima_PC1 = cen.dd[1], Optima_PC2 = cen.dd[2], Niche_width = areapl(as.matrix(dat[chull(dat$PC1, dat$PC2), c("PC1", "PC2")])) / areapl(as.matrix(data[[(i + 1)/2]][, c(2:3)][chull(data[[(i + 1)/2]][, c(2:3)]$PC1, data[[(i + 1)/2]][, c(2:3)]$PC2), c("PC1", "PC2")])))
    }
    return(df)
  })
}) %>%
  mutate(Life_form = gsub("_clim", "", Life_form)) %>%
  mutate(Life_form = factor(Life_form, levels = c("climber", "tree", "shrub", "herb"))) %>%
  filter(Niche_width != 0)

##### Objective 2: Niche optima vary with species association and across life forms #####
capture.output(summary(lm(Posterior_coefficients ~ Life_form, data = Niche)), file = "Posterior coefficients.doc")

# Plotting Niche optima across life forms
ggplot(Niche, aes(x = Optima_PC1, y = Optima_PC2, color = Life_form)) +
  geom_point(alpha=0.5) +
  labs(x = "PC1 (Mean Temperature of Driest Quarter)", y = "PC2 (Annual Temperature Range)") +
  theme_bw()

capture.output(summary(gam(Optima_PC1 ~ s(Posterior_coefficients) + factor(Life_form) + s(Posterior_coefficients, by = Life_form), data = Niche)), file = "results/Main/Niche optima.doc")

plots <- Niche[Niche[, 5] > -5, ]
plots$Life_form <- factor(plots$Life_form, levels = c("tree", "shrub", "herb", "climber"))

scale <- breaks(data = plots, x = "Posterior_coefficients", y = "Optima_PC1", group = "Life_form", ggplot = list(geom_point(alpha = 0.5), geom_smooth(method = "loess", se = FALSE, aes(group = Life_form))))
scale$X_axis <- c(-0.50, 0.575, 0.25)
scale$Y_axis <- c(-5, 7, 3)

# Plotting
ggsave(ggplot(plots, aes(x = Posterior_coefficients, y = Optima_PC1, color = Life_form)) + geom_point(alpha = 0.5) + geom_smooth(method = "loess", se = FALSE, aes(group = Life_form)) + scale_x_continuous(limits = c(scale[1, "X_axis"], scale[2, "X_axis"]), breaks = seq(scale[1, "X_axis"], scale[2, "X_axis"], scale[3, "X_axis"]), expand = expansion(mult = c(0.025, 0.025), add = c(0, 0))) + scale_y_continuous(limits = c(scale[1, "Y_axis"], scale[2, "Y_axis"]), breaks = seq(scale[1, "Y_axis"], scale[2, "Y_axis"], scale[3, "Y_axis"]), expand = expansion(mult = c(0.025, 0.075), add = c(0, 0))) + labs(x = "Species' association to climate", y = "Niche optima (PC1)") + scale_color_manual(labels = c("Tree", "Shrub", "Herb", "Climber"), values = c("#FF7043", "#1E88E5", "#66BB6A", "#7E57C2")) + Theme + theme(legend.text = element_text(size = 12*72.27/72), legend.direction = "horizontal", legend.position = c(0.5, 0.94), legend.background  = element_blank(), legend.title = element_blank()), file = paste0("results/Main/Optima_Association", ".tiff"), width = 15.92, height = 15.92/ar[1]*ar[2], units = "cm", dpi = 600)

##### Compute plot-level mean species' niche width #####
div <- 100
Species_estimates <- lapply(seq_along(data), function(i) {
  df <- data[[i]]
  for (l in 5:ncol(df)) {
    for (k in 1:nrow(df)) {
      if (df[k, l] == 0) {
        df[k, l] <- NA
      } else if (length(Niche[Niche[, 1] == colnames(df)[l], 3]) == 0) {
        df[k, l] <- NA
      } else {
        df[k, l] <- Niche[Niche[, 1] == colnames(df)[l], 3]
      }
    }
  }
  df$mean <- rowMeans(df[, 5:ncol(df)], na.rm = TRUE)
  return(df)
}) %>% set_names(names(data) %>% gsub("_clim", "", .))
Niche_width <- lapply(seq_along(data), function(i) {
  df <- data[[i]]
  for (l in 5:ncol(df)) {
    for (k in 1:nrow(df)) {
      if (df[k, l] == 0) {
        df[k, l] <- NA
      } else if (length(Niche[Niche[, 1] == colnames(df)[l], 7]) == 0) {
        df[k, l] <- NA
      } else {
        df[k, l] <- Niche[Niche[, 1] == colnames(df)[l], 7]
      }
    }
  }
  df$mean <- rowMeans(df[, 5:ncol(df)], na.rm = TRUE)
  return(df)
}) %>% set_names(names(Species_estimates))
Niche_optima <- lapply(seq_along(data), function(i) {
  df <- data[[i]]
  for (l in 5:ncol(df)) {
    for (k in 1:nrow(df)) {
      if (df[k, l] == 0) {
        df[k, l] <- NA
      } else if (length(Niche[Niche[, 1] == colnames(df)[l], 5]) == 0) {
        df[k, l] <- NA
      } else {
        df[k, l] <- Niche[Niche[, 1] == colnames(df)[l], 5]
      }
    }
  }
  df$mean <- rowMeans(df[, 5:ncol(df)], na.rm = TRUE)
  return(df)
}) %>% set_names(names(Species_estimates))

niche_width <- Niche_width[[1]][, c(1:4, ncol(Niche_width[[1]]))]
for (i in 2:length(Niche_width)) {
  niche_width <- cbind(niche_width, left_join(niche_width[, 1, drop = FALSE], Niche_width[[i]][, c(1, ncol(Niche_width[[i]]))], by = "Plot")[, 2])
}
colnames(niche_width)[5:ncol(niche_width)] <- names(Species_estimates)
niche_width$overall <- rowMeans(niche_width[, 5:ncol(niche_width)], na.rm = TRUE)

plots <- niche_width %>%
  select(1:4, ncol(.), 5:(ncol(.) - 1)) %>%
  tidyr::pivot_longer(cols = c(overall, tree, shrub, herb, climber), names_to = "Life_form", values_to = "Niche_width") %>%
  filter(!is.na(Niche_width)) %>%
  mutate(Life_form = factor(Life_form, levels = c("overall", "tree", "shrub", "herb", "climber"))) %>%
  arrange(across(c(5, 2))) %>%
  mutate(Life_form = factor(Life_form, levels = c("overall", "climber", "tree", "shrub", "herb"))) %>%
  data.frame()

capture.output(summary(gam(Niche_width ~ s(PC1) + factor(Life_form) + s(PC1, by = Life_form), data = plots[plots[, 5] != "overall", ])), file = "results/Main/Community niche width_gam.doc")

plots <- plots %>%
  group_by(Life_form) %>%
  mutate(across(2, ~ cut(., breaks = quantile(., probs = seq(0, 1, length.out = div + 1), na.rm = TRUE), include.lowest = TRUE), .names = "Quantile")) %>%
  group_by(across(c(Life_form, Quantile))) %>%
  summarise(across(2, ~ mean(c(min(.), max(.))), .names = "PC"),
            across(5, .fns = list(Niche_width_mean = mean, Niche_width_se = se))) %>%
  ungroup() %>%
  rename_with(~ c("Niche_width_mean", "Niche_width_se"), .cols = c(4, 5)) %>%
  select(-2) %>%
  data.frame()

plots <- plots[plots[, 1] != "overall", ]
plots$Life_form <- factor(plots$Life_form, levels = c("tree", "shrub", "herb", "climber"))

scale <- breaks(data = plots, x = "PC", y = "Niche_width_mean", group = "Life_form", ggplot = list(geom_point(alpha = 0.5), geom_smooth(method = "loess", se = FALSE, aes(group = Life_form))))
scale$X_axis <- c(-11, 10, 5)
scale$Y_axis <- c(0.4, 0.8, 0.1)

# Plotting
ggsave(ggplot(plots, aes(x = PC, y = Niche_width_mean, color = Life_form)) + geom_point(alpha = 0.5) + geom_smooth(method = "loess", se = FALSE, aes(group = Life_form)) + scale_x_continuous(limits = c(scale[1, "X_axis"], scale[2, "X_axis"]), breaks = seq(scale[1, "X_axis"] + 1, scale[2, "X_axis"], scale[3, "X_axis"]), expand = expansion(mult = c(0.025, 0.025), add = c(0, 0))) + scale_y_continuous(limits = c(scale[1, "Y_axis"], scale[2, "Y_axis"]), breaks = seq(scale[1, "Y_axis"], scale[2, "Y_axis"], scale[3, "Y_axis"]), expand = expansion(mult = c(0.025, 0.075), add = c(0, 0))) + labs(x = "Climatic gradient (PC1)", y = "Mean (plot-level) niche width") + scale_color_manual(labels = c("Tree", "Shrub", "Herb", "Climber"), values = c("#FF7043", "#1E88E5", "#66BB6A", "#7E57C2")) + Theme + theme(legend.text = element_text(size = 12*72.27/72), legend.direction = "horizontal", legend.position = c(0.5, 0.94), legend.background  = element_blank(), legend.title = element_blank()), file = paste0("results/Main/Species' niche width", ".tiff"), width = 15.92, height = 15.92/ar[1]*ar[2], units = "cm", dpi = 600)

ggplot(plots[plots[, 1] != "overall", ], aes(x = PC, y = Niche_width_mean, color = Life_form)) +
  geom_point(alpha = 0.5) +
  geom_smooth(method = "loess", se = FALSE, aes(group = Life_form)) +
  labs(x = "PC1", y = "Species' niche width") +
  theme_bw()

##### Objective 2: Plot-level mean niche width vary with species association and across life forms #####
capture.output(summary(gls(Niche_width ~ Optima_PC1 * Life_form, data = Niche)), file = "results/Main/Niche width.doc")
capture.output(summary(gam(Niche_width ~ s(Optima_PC1) + factor(Life_form) + s(Optima_PC1, by = Life_form), data = Niche)), file = "results/Main/Niche width_gam.doc")
capture.output(summary(lm(Niche_width ~ (Optima_PC1 * Life_form) + (Optima_PC1_sq * Life_form), data = Niche)), file = "results/Main/Niche width_lm.doc")

plots <- Niche
plots$Life_form <- factor(plots$Life_form, levels = c("tree", "shrub", "herb", "climber"))

scale <- breaks(data = plots, x = "Optima_PC1", y = "Niche_width", group = "Life_form", ggplot = list(geom_point(alpha = 0.5), geom_smooth(method = "loess", se = FALSE, aes(group = Life_form))))
scale$X_axis <- c(-10, 7, 4)
scale$Y_axis <- c(-0.05, 1, 0.25)

# Plotting
ggsave(ggplot(Niche, aes(x = Optima_PC1, y = Niche_width, color = Life_form)) + geom_point(alpha = 0.5) + geom_smooth(method = "loess", se = FALSE, aes(group = Life_form)) + scale_x_continuous(limits = c(scale[1, "X_axis"], scale[2, "X_axis"]), breaks = seq(scale[1, "X_axis"], scale[2, "X_axis"], scale[3, "X_axis"]), expand = expansion(mult = c(0.025, 0.025), add = c(0, 0))) + scale_y_continuous(limits = c(scale[1, "Y_axis"], scale[2, "Y_axis"]), breaks = seq(scale[1, "Y_axis"] + 0.05, scale[2, "Y_axis"], scale[3, "Y_axis"]), expand = expansion(mult = c(0.025, 0.15), add = c(0, 0))) + labs(x = "Niche optima (PC1)", y = "Niche width") + scale_color_manual(labels = c("Tree", "Shrub", "Herb", "Climber"), values = c("#FF7043", "#1E88E5", "#66BB6A", "#7E57C2")) + Theme + theme(legend.text = element_text(size = 12*72.27/72), legend.direction = "horizontal", legend.position = c(0.5, 0.94), legend.background  = element_blank(), legend.title = element_blank()), file = paste0("results/Main/Niche width_Optima", ".tiff"), width = 15.92, height = 15.92/ar[1]*ar[2], units = "cm", dpi = 600)

##### Objective 3: Plot-level species' association to climate #####
Beta_mean <- reduce(1:length(data), function(i, j) {
  left_join(i, lapply(seq(1, length(model_estimates) - 1, 2), function(j) {
    data.frame(Plot = model_estimates[[j]]$plot_mean_estimates[[1]][[1]][, 1], Value = model_estimates[[j]]$plot_mean_estimates[[1]][[1]][, 3])
  })[[j]], by = "Plot", suffix = c("", as.character(j)))
}, .init = cbind(model_estimates[[1]]$plot_mean_estimates[[1]][[1]][, 1:2], model_estimates[[1]]$plot_mean_estimates[[2]][[1]][, 2, drop = FALSE], model_estimates[[1]]$plot_mean_estimates[[3]][[1]][, 2, drop = FALSE])) %>%
  mutate(across(everything(), ~replace(., is.nan(.), NA))) %>%
  rename_at(vars(5:ncol(.)), ~c("tree", "shrub", "herb", "climber")) %>%
  mutate(Beta = rowMeans(select(., 5:ncol(.)), na.rm = TRUE)) %>%
  select(1:4, ncol(.), 5:(ncol(.) - 1)) %>%
  tidyr::pivot_longer(cols = c(Beta, tree, shrub, herb, climber), names_to = "Life_form", values_to = "Beta") %>%
  filter(!is.na(Beta)) %>%
  mutate(Life_form = factor(Life_form, levels = c("Beta", "tree", "shrub", "herb", "climber"))) %>%
  arrange(across(c(5, 2))) %>%
  mutate(Life_form = factor(Life_form, levels = c("Beta", "climber", "tree", "shrub", "herb"))) %>%
  data.frame()

capture.output(summary(gam(Beta ~ s(PC1) + factor(Life_form) + s(PC1, by = Life_form), data = Beta_mean[Beta_mean[, 5] != "Beta", ])), file = "results/Main/Species' association_gam.doc")

plots <- Beta_mean %>%
  group_by(Life_form) %>%
  mutate(across(2, ~ cut(., breaks = quantile(., probs = seq(0, 1, length.out = div + 1), na.rm = TRUE), include.lowest = TRUE), .names = "Quantile")) %>%
  group_by(across(c(Life_form, Quantile))) %>%
  summarise(across(2, ~ mean(c(min(.), max(.))), .names = "PC"),
            across(5, .fns = list(Beta_mean = mean, Beta_se = se))) %>%
  ungroup() %>%
  rename_with(~ c("Beta_mean", "Beta_se"), .cols = c(4, 5)) %>%
  select(-2) %>%
  data.frame()

plots <- plots[plots[, 1] != "Beta", ]
plots$Life_form <- factor(plots$Life_form, levels = c("tree", "shrub", "herb", "climber"))

scale <- breaks(data = plots, x = "PC", y = "Beta_mean", group = "Life_form", ggplot = list(geom_point(alpha = 0.5), geom_smooth(method = "loess", se = FALSE, aes(group = Life_form))))
scale$X_axis <- c(-11, 10, 5)
scale$Y_axis <- c(-0.3, 0.3, 0.15)

# Plotting
ggsave(ggplot(plots, aes(x = PC, y = Beta_mean, color = Life_form)) + geom_point(alpha = 0.5) + geom_smooth(method = "loess", se = FALSE, aes(group = Life_form)) + scale_x_continuous(limits = c(scale[1, "X_axis"], scale[2, "X_axis"]), breaks = seq(scale[1, "X_axis"] + 1, scale[2, "X_axis"], scale[3, "X_axis"]), expand = expansion(mult = c(0.025, 0.025), add = c(0, 0))) + scale_y_continuous(limits = c(scale[1, "Y_axis"], scale[2, "Y_axis"]), breaks = seq(scale[1, "Y_axis"], scale[2, "Y_axis"], scale[3, "Y_axis"]), expand = expansion(mult = c(0.025, 0.075), add = c(0, 0))) + labs(x = "Climatic gradient (PC1)", y = "Mean (plot-level) species' association") + scale_color_manual(labels = c("Tree", "Shrub", "Herb", "Climber"), values = c("#FF7043", "#1E88E5", "#66BB6A", "#7E57C2")) + Theme + theme(legend.text = element_text(size = 12*72.27/72), legend.direction = "horizontal", legend.position = c(0.5, 0.94), legend.background  = element_blank(), legend.title = element_blank()), file = paste0("results/Main/Species' association", ".tiff"), width = 15.92, height = 15.92/ar[1]*ar[2], units = "cm", dpi = 600)

##### Objective 4: Plot-level mean species richness #####
plan(multisession, workers = min(availableCores(), cores, length(model_predictions)))
message("Number of parallel cores: ", nbrOfWorkers())

result <- do.call(rbind, future_lapply(seq_along(model_predictions), function(i) {
  i_name <- names(model_predictions)[i]
  do.call(rbind, lapply(seq_along(model_predictions[[i]]$Occ_predict), function(j) {
    j_name <- names(model_predictions[[i]]$Occ_predict)[j]  # Get the name for 'j' before processing the dataframe
    model_predictions[[i]]$Occ_predict[[j]] %>%
      arrange(across(2)) %>%
      mutate(across(2, ~ cut(., breaks = quantile(., probs = seq(0, 1, length.out = div + 1), na.rm = TRUE), include.lowest = TRUE), .names = "Quantile")) %>%
      group_by(Quantile) %>%
      summarise(across(2, ~ mean(c(min(.), max(.))), .names = "PC"), Count = mean(rowSums(across(3:(ncol(.) - 1)) > 0))) %>%
      ungroup() %>% # Make sure to ungroup before adding i and j
      mutate(i = i_name, Component = j_name) %>%  # Add columns for i and j
      tidyr::separate(i, into = c("Life_form", "Model"), sep = "_") %>%
      data.frame()
  }))
}))

plan(sequential)

plots <- result[result$Model == "Clim" & result$Component == "PC1", -c(6:7)]
plots$Life_form <- factor(plots$Life_form, levels = c("Tree", "Shrub", "Herb", "Climber"))

scale <- breaks(data = plots, x = "PC", y = "Count", group = "Life_form", ggplot = list(geom_point(alpha = 0.5), geom_smooth(method = "loess", span = 0.3, se = FALSE, aes(group = Life_form))))
scale$X_axis <- c(-11, 10, 5)
scale$Y_axis <- c(0, 35, 10)

# Plotting
ggsave(ggplot(plots, aes(x = PC, y = Count, color = Life_form)) + geom_point(alpha = 0.5) + geom_smooth(method = "loess", span = 0.3, se = FALSE, aes(group = Life_form)) + scale_x_continuous(limits = c(scale[1, "X_axis"], scale[2, "X_axis"]), breaks = seq(scale[1, "X_axis"] + 1, scale[2, "X_axis"], scale[3, "X_axis"]), expand = expansion(mult = c(0.01, 0.01), add = c(0, 0))) + scale_y_continuous(limits = c(scale[1, "Y_axis"], scale[2, "Y_axis"]), breaks = seq(scale[1, "Y_axis"], scale[2, "Y_axis"], scale[3, "Y_axis"]), expand = expansion(mult = c(0.01, 0.01), add = c(0, 0))) + labs(x = "Climatic gradient (PC1)", y = "Mean (plot-level) species richness") + scale_color_manual(labels = c("Tree", "Shrub", "Herb", "Climber"), values = c("#FF7043", "#1E88E5", "#66BB6A", "#7E57C2")) + Theme + theme(legend.text = element_text(size = 12*72.27/72), legend.direction = "horizontal", legend.position.inside = c(0.5, 0.94), legend.background  = element_blank(), legend.title = element_blank()), file = paste0("results/Main/Predicted species richness_Grouped", ".tiff"), width = 15.92, height = 15.92/ar[1]*ar[2], units = "cm", dpi = 600)

##### Objective 4: Plot-level mean species co_occurrence #####
compute_co_occurrence <- function(row, x) {
  # Subset the matrix 'x' by these present species in both rows and columns
  present_species <- names(row[row > 0])
  occ <- x[rownames(x) %in% present_species, colnames(x) %in% present_species, drop = FALSE]
  
  # Exclude diagonal (a species doesn’t co-occur with itself)
  diag(occ) <- NA
  
  # Compute the average co-occurrence across present species
  avg_co_occ <- mean(rowMeans(occ, na.rm = TRUE), na.rm = TRUE)
  
  # If the mean is 0, set to NA; otherwise return that mean
  ifelse(avg_co_occ == 0, NA, avg_co_occ)
}

co_occurrence <- lapply(seq(1, length(model_diagnostics) - 1, 2), function(i) {
  data.frame(data[[(i + 1)/2]][, 1:4], co_occurrence = apply(data[[(i + 1)/2]][, 5:ncol(data[[(i + 1)/2]])], 1, compute_co_occurrence, model_diagnostics[[i]]$co_occurrence))
}) %>%
  set_names(c("tree", "shrub", "climber", "herb"))

co_occurrence <- bind_cols(co_occurrence[[1]], map2_dfc(co_occurrence[-1], 2:length(co_occurrence), ~ left_join(data.frame(Plot = co_occurrence[[1]][, 1]), data.frame(Plot = .x[, 1], co_occurrence = .x[, ncol(.x)]), by = "Plot")[, "co_occurrence", drop = FALSE])) %>%
  mutate(mean_co_occurrence = rowMeans(select(., starts_with("co_occurrence")), na.rm = TRUE)) %>%
  select(1:4, mean_co_occurrence, starts_with("co_occurrence")) %>%
  rename_at(vars(5:ncol(.)), ~ c("co_occurrence", "tree", "shrub", "herb", "climber")) %>%
  pivot_longer(cols = c("co_occurrence", "tree", "shrub", "herb", "climber"), names_to = "Life_form", values_to = "Co_occurrence") %>%
  filter(!is.na(Co_occurrence)) %>%
  mutate(Life_form = factor(Life_form, levels = c("co_occurrence", "climber", "tree", "shrub", "herb"))) %>%
  arrange(across(c(5, 2))) %>%
  mutate(Life_form = factor(Life_form, levels = c("co_occurrence", "climber", "tree", "shrub", "herb"))) %>%
  data.frame()

capture.output(summary(gam(Co_occurrence ~ s(PC1) + factor(Life_form) + s(PC1, by = Life_form), data = co_occurrence[co_occurrence[, 5] != "co_occurrence", ])), file = "results/Main/Species' co-occurrence_gam.doc")

plots <- co_occurrence %>%
  group_by(Life_form) %>%
  mutate(across(2, ~ cut(., breaks = quantile(., probs = seq(0, 1, length.out = div + 1), na.rm = TRUE), include.lowest = TRUE), .names = "Quantile")) %>%
  group_by(across(c(Life_form, Quantile))) %>%
  summarise(across(2, ~ mean(c(min(.), max(.))), .names = "PC"),
            across(5, .fns = list(Co_occurrence_mean = mean, Co_occurrence_se = se))) %>%
  ungroup() %>%
  rename_with(~ c("Co_occurrence_mean", "Co_occurrence_se"), .cols = c(4, 5)) %>%
  select(-2) %>%
  data.frame()

plots <- plots[plots[, 1] != "co_occurrence", ]
plots$Life_form <- factor(plots$Life_form, levels = c("tree", "shrub", "herb", "climber"))

scale <- breaks(data = plots, x = "PC", y = "Co_occurrence_mean", group = "Life_form", ggplot = list(geom_point(alpha = 0.5), geom_smooth(method = "loess", se = FALSE, aes(group = Life_form))))
scale$X_axis <- c(-11, 10, 5)
scale$Y_axis <- c(-0.05, 0.35, 0.10)

# Plotting
ggsave(ggplot(plots, aes(x = PC, y = Co_occurrence_mean, color = Life_form)) + geom_point(alpha = 0.5) + geom_smooth(method = "loess", se = FALSE, aes(group = Life_form)) + scale_x_continuous(limits = c(scale[1, "X_axis"], scale[2, "X_axis"]), breaks = seq(scale[1, "X_axis"] + 1, scale[2, "X_axis"], scale[3, "X_axis"]), expand = expansion(mult = c(0.025, 0.025), add = c(0, 0))) + scale_y_continuous(limits = c(scale[1, "Y_axis"], scale[2, "Y_axis"]), breaks = seq(scale[1, "Y_axis"], scale[2, "Y_axis"], scale[3, "Y_axis"]), expand = expansion(mult = c(0.025, 0.075), add = c(0, 0))) + labs(x = "Climatic gradient (PC1)", y = "Mean (plot-level) species' co-occurrence") + scale_color_manual(labels = c("Tree", "Shrub", "Herb", "Climber"), values = c("#FF7043", "#1E88E5", "#66BB6A", "#7E57C2")) + Theme + theme(legend.text = element_text(size = 12*72.27/72), legend.direction = "horizontal", legend.position.inside = c(0.5, 0.94), legend.background  = element_blank(), legend.title = element_blank()), file = paste0("results/Main/Species' co-occurrence", ".tiff"), width = 15.92, height = 15.92/ar[1]*ar[2], units = "cm", dpi = 600)

# Save output to .RData
save.image(".RData")
